export interface IElement {
    tagName: string,
    className: string,
    attributes?: { [index: string]: any; }
}