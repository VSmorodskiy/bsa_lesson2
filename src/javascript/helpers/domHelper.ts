import { IElement } from './IElement';

export const createElement = ({ tagName, className, attributes } : IElement): Element => {
    const element = document.createElement(tagName);
  
    if (className) {
      const classNames = className.split(' ').filter(Boolean);
      element.classList.add(...classNames);
    }
  
    if(typeof(attributes) !== 'undefined') {
        Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));
    }
  
    return element;
  }